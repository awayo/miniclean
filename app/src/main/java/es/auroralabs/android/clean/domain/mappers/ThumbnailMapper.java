package es.auroralabs.android.clean.domain.mappers;


import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.ComicSummaryData;
import es.auroralabs.android.clean.datasource.model.ThumbnailData;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.android.clean.domain.entities.ThumbnailEntity;
import es.auroralabs.tools.glifo.mappers.base.BasePropertiesMapper;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ThumbnailMapper extends BasePropertiesMapper<ThumbnailEntity, ThumbnailData> {

    public ThumbnailMapper() {
        super(ThumbnailEntity.class, ThumbnailData.class);
    }

    @Override
    public ThumbnailData fromEntity(ThumbnailEntity entity) {
        ThumbnailData ThumbnailData = super.fromEntity(entity);
        return ThumbnailData;
    }

    @Override
    public ThumbnailEntity toEntity(ThumbnailData thumbnailData) {
        ThumbnailEntity thumbnailEntity = super.toEntity(thumbnailData);
        return thumbnailEntity;
    }

    public List<ThumbnailEntity> toEntity(List<ThumbnailData> characters) {
        List<ThumbnailEntity> list = new ArrayList<>();
        for (ThumbnailData data : characters) {
            list.add(toEntity(data));
        }

        return list;
    }

    public List<ThumbnailData> fromEntity(List<ThumbnailEntity> characters) {
        List<ThumbnailData> list = new ArrayList<>();
        for (ThumbnailEntity data : characters) {
            list.add(fromEntity(data));
        }

        return list;
    }

}
