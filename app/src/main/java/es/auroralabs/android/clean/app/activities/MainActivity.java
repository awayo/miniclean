package es.auroralabs.android.clean.app.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.app.adapters.CharactersAdapter;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.NetModule;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.usecases.characters.GetCharactersUseCase;

public class MainActivity extends AppCompatActivity {

    List<CharacterEntity> characterEntities;
    Integer page = 0;
    @BindView(R.id.swipeContainer)
    SwipeRefreshLayout swipeContainer;

    CharactersAdapter charactersAdapter;
    LinearLayoutManager linearLayoutManager;

    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    private BroadcastReceiver sessionBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            closeSession();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // Register receiver to close the session
        LocalBroadcastManager.getInstance(this).registerReceiver(sessionBroadcastReceiver, new IntentFilter(NetModule.CLOSE_SESSION_INTENT_FILTER));

        prepareView();

        fetchData(0);
    }

    @Override
    protected void onDestroy() {

        // Unregister receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(sessionBroadcastReceiver);

        super.onDestroy();
    }

    private void prepareView() {
        ButterKnife.bind(this);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        characterEntities = new ArrayList<>();

        charactersAdapter = new CharactersAdapter(characterEntities);

        recyclerView.setAdapter(charactersAdapter);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.this.page = 0;
                characterEntities.clear();
                charactersAdapter.notifyDataSetChanged();
                fetchData(page);
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            fetchData(1);
                        }
                    }
                }
            }
        });
    }

    private void fetchData(final int pageIncrement) {
        this.page += pageIncrement;

        progressBar.setVisibility(View.VISIBLE);
        GetCharactersUseCase getCharactersUseCase = new GetCharactersUseCase(this, Strategy.SERVICE_NO_CACHE);
        getCharactersUseCase.retrieveCharacterList(this.page, new Callback<List<CharacterEntity>>() {
            @Override
            public void onSuccess(List<CharacterEntity> characters) {
                characterEntities.addAll(characters);
                charactersAdapter.notifyDataSetChanged();
                loading = true;
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(Throwable t) {
                MainActivity.this.page = MainActivity.this.page > 0 ? MainActivity.this.page - pageIncrement: 0;
                loading = true;
                swipeContainer.setRefreshing(false);
                progressBar.setVisibility(View.VISIBLE);
                Log.d("Clean - Error", t.toString());
            }
        });
    }

    private void closeSession() {

        // Go back to login
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);

        finish();
    }
}
