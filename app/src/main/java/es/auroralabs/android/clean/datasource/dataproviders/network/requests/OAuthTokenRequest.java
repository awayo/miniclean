package es.auroralabs.android.clean.datasource.dataproviders.network.requests;

import android.support.annotation.NonNull;

import es.auroralabs.android.clean.BuildConfig;


/**
 * Created by jmtm on 27/6/17.
 */
public class OAuthTokenRequest {

    private static final String GRANT_TYPE = "password";

    private String grantType = GRANT_TYPE;
    private String clientId = BuildConfig.API_CLIENT_ID;
    private String clientSecret = BuildConfig.API_CLIENT_SECRET;
    private String username;
    private String password;

    public OAuthTokenRequest(@NonNull String username, @NonNull String password) {
        this.username = username;
        this.password = password;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
