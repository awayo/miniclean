package es.auroralabs.android.clean.datasource.cache;

import java.util.HashMap;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public class SimpleMemoryCache<T> implements Cache<T> {

    HashMap<String, T> cacheMap;

    public SimpleMemoryCache() {
        cacheMap = new HashMap<>();
    }

    public T get(String key) {
        return cacheMap.get(key);
    }

    public boolean add(String key, T object) {
        cacheMap.put(key, object);
        return true;
    }

    public boolean remove(String key) {
        return cacheMap.remove(key) != null;
    }

}
