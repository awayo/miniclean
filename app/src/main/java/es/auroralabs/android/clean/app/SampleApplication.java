package es.auroralabs.android.clean.app;

import android.app.Application;

import es.auroralabs.aurversionmanager.VersionManager;

/**
 * Created by jmtm on 3/1/18.
 */

public class SampleApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // TODO: Initialize third party libraries here

        VersionManager vm = VersionManager.getInstance(this);
    }
}
