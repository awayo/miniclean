package es.auroralabs.android.clean.datasource.dataproviders;

import android.content.Context;

import java.lang.ref.WeakReference;

import es.auroralabs.android.clean.datasource.dataproviders.mock.ComicMockRepositoryImpl;
import es.auroralabs.android.clean.datasource.dataproviders.network.ComicNetworkRepositoryImpl;
import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.repository.ComicRepository;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public class ComicDataProvider implements ComicRepository {

    private static ComicDataProvider instance;

    private WeakReference<Context> weakContext;
    private Strategy strategy;
    private Object[] configurator;

    private ComicDataProvider() {

    }

    synchronized public static ComicDataProvider getInstance(Context context, Strategy strategy, Object... configurator) {
        if (instance == null || instance.strategy != strategy) {
            instance = new ComicDataProvider();
            instance.strategy = strategy;
        }
        instance.weakContext = new WeakReference<Context>(context);
        instance.configurator = configurator;

        return instance;
    }

    @Override
    public void comic(String comicId, Callback<ComicData> callback) {
        if (callback != null) {
            if (weakContext != null && weakContext.get() != null) {
                if (strategy != null) {
                    switch (strategy) {
                        case MOCK:
                            ComicMockRepositoryImpl mockRepository = new ComicMockRepositoryImpl(configurator);
                            mockRepository.comic(comicId, callback);
                            break;
                        case SERVICE_NO_CACHE:
                            ComicNetworkRepositoryImpl noCacheNetworkRepository = new ComicNetworkRepositoryImpl(weakContext.get(), false, configurator);
                            noCacheNetworkRepository.comic(comicId, callback);
                            break;
                        case SERVICE_CACHE_FIRST:
                            ComicNetworkRepositoryImpl cacheNetworkRepository = new ComicNetworkRepositoryImpl(weakContext.get(), true, configurator);
                            cacheNetworkRepository.comic(comicId, callback);
                            break;
                    }
                }
            } else {
                callback.onError(new Exception("Context reference is null"));
            }
        }
    }
}
