package es.auroralabs.android.clean.datasource.dataproviders.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

import es.auroralabs.android.clean.datasource.model.OAuthData;

/**
 * Created by jmtm on 27/6/17.
 */

public class Preferences {

    private static final String FILE_NAME = "sharedPreferences";

    private static String ACCESS_TOKEN = "access_token";
    private static String EXPIRES_IN = "expires_in";
    private static String REFRESH_TOKEN = "refresh_token";
    private static final String ACCESS_TOKEN_TIME = "access_token_time";

    private static final String DATA_KEY = "device_id";

    private static final String USER_ID = "user_id";

    private static final String MESSAGES = "messages";

    private static Preferences instance;

    private SharedPreferences sharedPreferences;

    private Preferences() {

    }

    public static Preferences getInstance(Context context) {
        if (instance == null) {
            instance = new Preferences();

            instance.init(context);
        }

        return instance;
    }

    private void init(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public boolean saveOAuthToken(OAuthData oAuthData) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(ACCESS_TOKEN, oAuthData.getAccessToken());
            editor.putInt(EXPIRES_IN, oAuthData.getExpiresIn());
            editor.putString(REFRESH_TOKEN, oAuthData.getRefreshToken());
            editor.putLong(ACCESS_TOKEN_TIME, new Date().getTime());

            return editor.commit();
        }

        return false;
    }

    public OAuthData getOAuthToken() {

        if (sharedPreferences != null) {

            OAuthData oAuthData = new OAuthData();

            oAuthData.setAccessToken(sharedPreferences.getString(ACCESS_TOKEN, ""));
            oAuthData.setExpiresIn(sharedPreferences.getInt(EXPIRES_IN, 0));
            oAuthData.setRefreshToken(sharedPreferences.getString(REFRESH_TOKEN, ""));

            return oAuthData;
        }

        return null;
    }

    public boolean logout() {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.remove(ACCESS_TOKEN);
            editor.remove(REFRESH_TOKEN);
            editor.remove(EXPIRES_IN);
            editor.remove(USER_ID);
            editor.remove(ACCESS_TOKEN_TIME);
            editor.remove(MESSAGES);

            return editor.commit();
        }

        return false;
    }

    public String getData() {
        if (sharedPreferences != null) {

            // TODO: Read your data here
            return sharedPreferences.getString(DATA_KEY, "");
        }

        return "";
    }

    public boolean saveData(String data) {
        if (sharedPreferences != null) {
            SharedPreferences.Editor editor = sharedPreferences.edit();

            // TODO save your data here
            editor.putString(DATA_KEY, data);

            return editor.commit();
        }

        return false;
    }
}
