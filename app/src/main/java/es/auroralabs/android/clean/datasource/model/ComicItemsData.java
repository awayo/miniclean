package es.auroralabs.android.clean.datasource.model;

import java.util.List;

import es.auroralabs.tools.glifo.model.base.ModelData;


/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public class ComicItemsData extends ModelData {
    List<ComicSummaryData> items;

    public List<ComicSummaryData> getItems() {
        return items;
    }

    public void setItems(List<ComicSummaryData> items) {
        this.items = items;
    }
}
