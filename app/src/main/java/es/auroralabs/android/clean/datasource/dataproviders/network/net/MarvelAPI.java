package es.auroralabs.android.clean.datasource.dataproviders.network.net;

import es.auroralabs.android.clean.datasource.dataproviders.network.responses.CharacterWrapperResponse;
import es.auroralabs.android.clean.datasource.dataproviders.network.responses.ComicWrapperResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public interface MarvelAPI {

    @GET("/v1/public/characters")
    Call<CharacterWrapperResponse> getCharacters(@Query("limit") Integer limit, @Query("offset") Integer offset);

    @GET("/v1/public/characters/{id}")
    Call<CharacterWrapperResponse> getCharacter(@Path("id") String characterId);

    @GET("/v1/public/comics/{id}")
    Call<ComicWrapperResponse> getComic(@Path("id") String comicId);
}


