package es.auroralabs.android.clean.domain.usecases.characters;

import android.content.Context;

import java.util.List;

import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.datasource.dataproviders.CharacterDataProvider;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.mappers.CharacterMapper;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class GetCharactersUseCase {

    CharacterRepository characterRepository;

    public GetCharactersUseCase(Context context, Strategy strategy) {
        this.characterRepository = CharacterDataProvider.getInstance(context, strategy);
    }

    public void retrieveCharacterList(Integer page, final Callback<List<CharacterEntity>> callback) {

        if (callback != null) {
            characterRepository.characterList(page, new Callback<List<CharacterData>>() {
                @Override
                public void onSuccess(List<CharacterData> characters) {
                    CharacterMapper charactersMapper = new CharacterMapper();
                    callback.onSuccess(charactersMapper.toEntity(characters));
                }

                @Override
                public void onError(Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }
}
