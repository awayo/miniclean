package es.auroralabs.android.clean.app.viewholders;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.app.activities.CharacterDetailActivity;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public class CharacterViewHolder extends RecyclerView.ViewHolder {

    CharacterEntity characterEntity;

    TextView name;
    TextView comics;
    ImageView thumbnail;

    public CharacterViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.text);
        comics = (TextView) itemView.findViewById(R.id.comics);
        thumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);

    }

    public void bind(final CharacterEntity characterEntity) {
        this.characterEntity = characterEntity;
        name.setText(this.characterEntity.getName());
        String aux = "";
        for (ComicSummaryEntity comicEntity : characterEntity.getComicSummary()) {
            aux += comicEntity.getName() + "\r\n";
        }

        if (aux.isEmpty()) {
            aux = "No comics!";
        }

        Glide.with(this.itemView.getContext()).load(this.characterEntity.getThumbnail().getURL())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Palette palette = Palette.from(drawableToBitmap(resource)).generate();
                        int defaultColor = ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary);
                        name.setTextColor(palette.getLightVibrantColor(defaultColor));
                        comics.setTextColor(palette.getLightMutedColor(defaultColor));
                        itemView.setBackgroundColor(palette.getDarkMutedColor(defaultColor));
                        return false;
                    }
                })
                .into(thumbnail);

        comics.setText(aux);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), CharacterDetailActivity.class);

                intent.putExtra(CharacterDetailActivity.CHARACTER_ID_BUNDLE, characterEntity);

                itemView.getContext().startActivity(intent);
            }
        });
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }
}
