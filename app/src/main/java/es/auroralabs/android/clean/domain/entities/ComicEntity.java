package es.auroralabs.android.clean.domain.entities;

import android.os.Parcel;
import android.os.Parcelable;

import es.auroralabs.tools.glifo.entities.base.ModelEntity;


/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicEntity extends ModelEntity implements Parcelable {
    String title;
    ThumbnailEntity thumbnail;
    String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ThumbnailEntity getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailEntity thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public int describeContents() {
        return this.title.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeString(this.description);
    }

    public ComicEntity() {
    }

    protected ComicEntity(Parcel in) {
        this.title = in.readString();
        this.thumbnail = in.readParcelable(ThumbnailEntity.class.getClassLoader());
        this.description = in.readString();
    }

    public static final Parcelable.Creator<ComicEntity> CREATOR = new Parcelable.Creator<ComicEntity>() {
        @Override
        public ComicEntity createFromParcel(Parcel source) {
            return new ComicEntity(source);
        }

        @Override
        public ComicEntity[] newArray(int size) {
            return new ComicEntity[size];
        }
    };
}
