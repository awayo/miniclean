package es.auroralabs.android.clean.datasource.model;

import es.auroralabs.tools.glifo.model.base.ModelData;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ThumbnailData extends ModelData {
    String path;
    String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
