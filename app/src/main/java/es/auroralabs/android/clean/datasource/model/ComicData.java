package es.auroralabs.android.clean.datasource.model;

import es.auroralabs.tools.glifo.model.base.ModelData;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public class ComicData extends ModelData {
    String title;
    String description;
    ThumbnailData thumbnail;
    Integer issueNumber;
    Integer pageCount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ThumbnailData getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailData thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getIssueNumber() {
        return issueNumber;
    }

    public void setIssueNumber(Integer issueNumber) {
        this.issueNumber = issueNumber;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }
}
