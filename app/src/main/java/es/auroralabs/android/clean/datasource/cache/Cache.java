package es.auroralabs.android.clean.datasource.cache;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public interface Cache<T> {
    T get(String key);
    boolean add(String key, T object);
    boolean remove(String key);
}
