package es.auroralabs.android.clean.datasource.dataproviders;

import android.content.Context;

import java.lang.ref.WeakReference;

import es.auroralabs.android.clean.datasource.dataproviders.preferences.PreferencesRepositoryImpl;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.domain.repository.PreferencesRepository;

/**
 * Created by jmtm on 29/6/17.
 */

public class PreferencesDataProvider implements PreferencesRepository {

    private static PreferencesDataProvider instance;

    private WeakReference<Context> weakContext;

    private PreferencesDataProvider() {

    }

    synchronized public static PreferencesDataProvider getInstance(Context context) {
        if (instance == null) {
            instance = new PreferencesDataProvider();

        }
        instance.weakContext = new WeakReference<>(context);

        return instance;
    }

    @Override
    public boolean saveOAuthToken(OAuthData oauthData) {
        if (weakContext != null && weakContext.get() != null) {
            PreferencesRepositoryImpl preferencesRepository = new PreferencesRepositoryImpl(weakContext.get());
            return preferencesRepository.saveOAuthToken(oauthData);
        }

        return false;
    }

    @Override
    public boolean saveData(String data) {
        if (weakContext != null && weakContext.get() != null) {
            PreferencesRepositoryImpl preferencesRepository = new PreferencesRepositoryImpl(weakContext.get());
            return preferencesRepository.saveData(data);
        }

        return false;
    }

    @Override
    public String getData() {
        if (weakContext != null && weakContext.get() != null) {
            PreferencesRepositoryImpl preferencesRepository = new PreferencesRepositoryImpl(weakContext.get());
            return preferencesRepository.getData();
        }

        return "";
    }
}
