package es.auroralabs.android.clean.domain.mappers;

import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.model.ComicItemsData;
import es.auroralabs.android.clean.datasource.model.ComicSummaryData;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.tools.glifo.mappers.base.BasePropertiesMapper;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicSummaryMapper extends BasePropertiesMapper<ComicSummaryEntity, ComicSummaryData> {

    public ComicSummaryMapper() {
        super(ComicSummaryEntity.class, ComicSummaryData.class);
    }

    @Override
    public ComicSummaryData fromEntity(ComicSummaryEntity comicSummaryEntity) {
        ComicSummaryData comic = super.fromEntity(comicSummaryEntity);
        return comic;
    }

    @Override
    public ComicSummaryEntity toEntity(ComicSummaryData comic) {
        ComicSummaryEntity ComicSummaryEntity = super.toEntity(comic);
        return ComicSummaryEntity;
    }

    public List<ComicSummaryEntity> toEntity(List<ComicSummaryData> characters) {
        List<ComicSummaryEntity> list = new ArrayList<>();
        for (ComicSummaryData data : characters) {
            list.add(toEntity(data));
        }

        return list;
    }

    public ComicItemsData fromEntity(List<ComicSummaryEntity> characters) {
        ComicItemsData comicResponse = new ComicItemsData();
        List<ComicSummaryData> list = new ArrayList<>();
        for (ComicSummaryEntity data : characters) {
            list.add(fromEntity(data));
        }

        comicResponse.setItems(list);
        return comicResponse;
    }

}
