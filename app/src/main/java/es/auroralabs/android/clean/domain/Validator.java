package es.auroralabs.android.clean.domain;

import android.text.TextUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Pattern;

/**
 * Created by jmtm on 4/7/17.
 */

public class Validator {

    private static String PASSWORD_PATTERN = "(\\p{Alnum}|\\p{Punct}){8,}"; // 8 alphanumeric characters
    private static String NUMERIC_PATTERN = "[0-9]+"; // Numeric chars

    public static boolean validateRequiredString(String string) {
        return !TextUtils.isEmpty(string);
    }

    public static boolean validateMaxLengthString(String string, int maxLength) {
        return !TextUtils.isEmpty(string) && string.length() <= maxLength;
    }

    public static boolean validateEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean validatePassword(String password) {
        return Pattern.matches(PASSWORD_PATTERN, password);
    }

    public static boolean validateOptionalPassword(String password) {
        if (!TextUtils.isEmpty(password)) {
            return Pattern.matches(PASSWORD_PATTERN, password);
        }

        return true;
    }

    public static boolean validateConfirmPassword(String password, String confirmPassword) {
        if (password != null && confirmPassword != null) {
            return password.equals(confirmPassword);
        }

        return false;
    }

    public static boolean validateNumeric(String number) {
        return Pattern.matches(NUMERIC_PATTERN, number);
    }
}
