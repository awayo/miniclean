package es.auroralabs.android.clean.domain.usecases;

import android.content.Context;

import es.auroralabs.android.clean.datasource.dataproviders.PreferencesDataProvider;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.repository.PreferencesRepository;

/**
 * Created by jmtm on 12/7/17.
 */

public class PreferencesUseCase {

    private PreferencesRepository preferencesRepository;

    public PreferencesUseCase(Context context) {
        preferencesRepository = PreferencesDataProvider.getInstance(context);
    }

    public void readFromPreferences(final Callback<String> callback) {
        if (callback != null) {
            callback.onSuccess(preferencesRepository.getData());
        }
    }

    public void saveInPreferences(String data, final Callback<Boolean> callback) {
        if (callback != null) {
            callback.onSuccess(preferencesRepository.saveData(data));
        }
    }
}
