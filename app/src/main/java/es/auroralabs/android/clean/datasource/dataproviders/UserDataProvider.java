package es.auroralabs.android.clean.datasource.dataproviders;

import android.content.Context;

import java.lang.ref.WeakReference;

import es.auroralabs.android.clean.datasource.dataproviders.mock.UserMockRepositoryImpl;
import es.auroralabs.android.clean.datasource.dataproviders.network.UserNetworkRepositoryImpl;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.repository.UserRepository;

/**
 * Created by jmtm on 29/6/17.
 */

public class UserDataProvider implements UserRepository {

    private static UserDataProvider instance;

    private WeakReference<Context> weakContext;
    private Strategy strategy;
    private Object[] configurator;

    private UserDataProvider() {
    }

    synchronized public static UserDataProvider getInstance(Context context, Strategy strategy, Object[] configurator) {
        if (instance == null || instance.strategy != strategy) {
            instance = new UserDataProvider();
            instance.strategy = strategy;
        }
        instance.weakContext = new WeakReference<>(context);
        instance.configurator = configurator;

        return instance;
    }

    @Override
    public void login(String email, String password, Callback<OAuthData> callBack) {
        if (callBack != null) {
            if (weakContext != null && weakContext.get() != null) {
                if (strategy != null) {
                    switch (strategy) {
                        case MOCK:
                            UserMockRepositoryImpl mockRepository = new UserMockRepositoryImpl(configurator);
                            mockRepository.login(email, password, callBack);
                            break;
                        case SERVICE_NO_CACHE:
                        case SERVICE_CACHE_FIRST:
                            UserNetworkRepositoryImpl networkRepository = new UserNetworkRepositoryImpl(weakContext.get(), configurator);
                            networkRepository.login(email, password, callBack);
                            break;
                    }
                }
            } else {
                callBack.onError(new Exception("Context instance is null"));
            }
        }
    }

    @Override
    public void signUp(String name, String lastName, String email, String phoneNumber, String password, String confirmPassword, Callback<OAuthData> callBack) {
        if (callBack != null) {
            if (weakContext != null && weakContext.get() != null) {
                if (strategy != null) {
                    switch (strategy) {
                        case MOCK:
                            UserMockRepositoryImpl mockRepository = new UserMockRepositoryImpl(configurator);
                            mockRepository.signUp(name, lastName, email, phoneNumber, password, confirmPassword, callBack);
                            break;
                        case SERVICE_NO_CACHE:
                        case SERVICE_CACHE_FIRST:
                            UserNetworkRepositoryImpl networkRepository = new UserNetworkRepositoryImpl(weakContext.get(), configurator);
                            networkRepository.signUp(name, lastName, email, phoneNumber, password, confirmPassword, callBack);
                            break;
                    }
                }
            } else {
                callBack.onError(new Exception("Context instance is null"));
            }
        }
    }
}
