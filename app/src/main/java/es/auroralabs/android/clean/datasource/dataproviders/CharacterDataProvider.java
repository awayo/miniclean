package es.auroralabs.android.clean.datasource.dataproviders;

import android.content.Context;

import java.lang.ref.WeakReference;
import java.util.List;

import es.auroralabs.android.clean.datasource.dataproviders.mock.CharacterMockRepositoryImpl;
import es.auroralabs.android.clean.datasource.dataproviders.network.CharacterNetworkRepositoryImpl;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public class CharacterDataProvider implements CharacterRepository {

    private static CharacterDataProvider instance;

    private Strategy strategy;
    private WeakReference<Context> weakContext;
    private Object[] configurator;

    private CharacterDataProvider() {

    }

    synchronized public static CharacterDataProvider getInstance(Context context, Strategy strategy, Object... configurator) {
        if (instance == null || instance.strategy != strategy) {
            instance = new CharacterDataProvider();
            instance.strategy = strategy;
        }
        instance.weakContext = new WeakReference<>(context);
        instance.configurator = configurator;

        return instance;
    }

    @Override
    public void characterList(Integer page, Callback<List<CharacterData>> callback) {
        if (callback != null) {
            if (weakContext != null && weakContext.get() != null) {
                if (strategy != null) {
                    switch (strategy) {
                        case MOCK:
                            CharacterMockRepositoryImpl mockRepository = new CharacterMockRepositoryImpl(configurator);
                            mockRepository.characterList(page, callback);
                            break;
                        case SERVICE_NO_CACHE:
                            CharacterNetworkRepositoryImpl noCacheNetworkRepository = new CharacterNetworkRepositoryImpl(weakContext.get(), false, configurator);
                            noCacheNetworkRepository.characterList(page, callback);
                            break;
                        case SERVICE_CACHE_FIRST:
                            CharacterNetworkRepositoryImpl cacheNetworkRepository = new CharacterNetworkRepositoryImpl(weakContext.get(), true, configurator);
                            cacheNetworkRepository.characterList(page, callback);
                            break;
                    }
                }
            } else {
                callback.onError(new Exception("Context instance is null"));
            }
        }
    }

    @Override
    public void character(String characterId, Callback<CharacterData> callback) {
        if (callback != null) {
            if (weakContext != null && weakContext.get() != null) {
                if (strategy != null) {
                    switch (strategy) {
                        case MOCK:
                            CharacterMockRepositoryImpl mockRepository = new CharacterMockRepositoryImpl();
                            mockRepository.character(characterId, callback);
                            break;
                        case SERVICE_NO_CACHE:
                            CharacterNetworkRepositoryImpl noCacheNetworkRepository = new CharacterNetworkRepositoryImpl(weakContext.get(), false, configurator);
                            noCacheNetworkRepository.character(characterId, callback);
                            break;
                        case SERVICE_CACHE_FIRST:
                            CharacterNetworkRepositoryImpl cacheNetworkRepository = new CharacterNetworkRepositoryImpl(weakContext.get(), true, configurator);
                            cacheNetworkRepository.character(characterId, callback);
                            break;
                    }

                }
            } else {
                callback.onError(new Exception("Context instance is null"));
            }
        }
    }
}
