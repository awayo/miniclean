package es.auroralabs.android.clean.datasource.dataproviders.mock;


import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.model.ComicItemsData;
import es.auroralabs.android.clean.datasource.model.ThumbnailData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

//stub this one can be disk access, cloud access, SimpleQL, etc etc
public class CharacterMockRepositoryImpl {


    public CharacterMockRepositoryImpl(Object... configurator) {
        // Get configurator data here if needed
    }

    public void characterList(Integer page, Callback<List<CharacterData>> callback) {
        List<CharacterData> list = new ArrayList<>();

        CharacterData characterData = new CharacterData();
        characterData.setId("1212");
        characterData.setName("12asdasdasd12");
        characterData.setComicSummary(new ComicItemsData());

        ThumbnailData thumbnailData = new ThumbnailData();

        thumbnailData.setExtension("jpg");
        thumbnailData.setPath("http://google.es/algo");

        characterData.setThumbnail(thumbnailData);

        list.add(characterData);

        callback.onSuccess(list);
    }

    public void character(final String characterId, Callback<CharacterData> callback) {
        CharacterData characterData = new CharacterData();
        characterData.setId("1212");
        characterData.setName("12asdasdasd12");
        characterData.setComicSummary(new ComicItemsData());
        callback.onSuccess(characterData);
    }

}
