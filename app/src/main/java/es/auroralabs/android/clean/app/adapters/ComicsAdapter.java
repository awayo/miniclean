package es.auroralabs.android.clean.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.android.clean.app.viewholders.ComicSummaryViewHolder;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicsAdapter extends RecyclerView.Adapter<ComicSummaryViewHolder> {

    List<ComicSummaryEntity> list;

    public ComicsAdapter(List<ComicSummaryEntity> list) {
        this.list = list;
    }

    @Override
    public ComicSummaryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comic_cell, parent, false);
        return new ComicSummaryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ComicSummaryViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
