package es.auroralabs.android.clean.datasource.dataproviders.mock;

import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by jmtm on 22/12/17.
 */

public class ComicMockRepositoryImpl {

    public ComicMockRepositoryImpl(Object... configurators) {
        // Get configurator data here if needed
    }

    public void comic(String comicId, Callback<ComicData> callback) {
        // Does nothing
    }
}
