package es.auroralabs.android.clean.domain.repository;


import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by jmtm on 29/6/17.
 */

public interface UserRepository extends Repository {

    void login(String email, String password, Callback<OAuthData> callBack);

    void signUp(String name, String lastName, String email, String phoneNumber, String password, String confirmPassword, Callback<OAuthData> callBack);
}
