package es.auroralabs.android.clean.domain.mappers;


import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.tools.glifo.mappers.base.BasePropertiesMapper;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class CharacterMapper extends BasePropertiesMapper<CharacterEntity, CharacterData> {

    public CharacterMapper() {
        super(CharacterEntity.class, CharacterData.class);
    }

    @Override
    public CharacterData fromEntity(CharacterEntity characterEntity) {
        CharacterData character = super.fromEntity(characterEntity);
        ComicSummaryMapper comicsMapper = new ComicSummaryMapper();
        character.setComicSummary(comicsMapper.fromEntity(characterEntity.getComicSummary()));
        return character;
    }

    @Override
    public CharacterEntity toEntity(CharacterData character) {
        CharacterEntity characterEntity = super.toEntity(character);
        characterEntity.setType(CharacterEntity.CharacterType.getCharacterType(character.getType()));
        ComicSummaryMapper comicsMapper = new ComicSummaryMapper();
        characterEntity.setComicSummary(comicsMapper.toEntity(character.getComicSummary().getItems()));
        return characterEntity;
    }

    public List<CharacterEntity> toEntity(List<CharacterData> characters) {
        List<CharacterEntity> list = new ArrayList<>();
        for (CharacterData data : characters) {
            list.add(toEntity(data));
        }

        return list;
    }

    public List<CharacterData> fromEntity(List<CharacterEntity> characters) {
        List<CharacterData> list = new ArrayList<>();
        for (CharacterEntity data : characters) {
            list.add(fromEntity(data));
        }

        return list;
    }

}
