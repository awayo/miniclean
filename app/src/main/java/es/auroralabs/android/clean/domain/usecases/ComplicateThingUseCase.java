package es.auroralabs.android.clean.domain.usecases;

import android.content.Context;

import java.util.List;

import es.auroralabs.android.clean.datasource.dataproviders.CharacterDataProvider;
import es.auroralabs.android.clean.datasource.dataproviders.ComicDataProvider;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.android.clean.domain.mappers.ComicMapper;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;
import es.auroralabs.android.clean.domain.repository.ComicRepository;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComplicateThingUseCase {

    private ComicRepository comicRepository;
    private CharacterRepository characterRepository;

    private List<CharacterData> characterDatas;
    private ComicData comicData;

    private Callback<ComicEntity> callback;

    public ComplicateThingUseCase(Context context, Strategy strategy) {
        this.comicRepository = ComicDataProvider.getInstance(context, strategy);
        this.characterRepository = CharacterDataProvider.getInstance(context, strategy);
    }

    public void retrieveComplicatedThing(final Callback<ComicEntity> callback) {

        if (callback != null) {
            this.callback = callback;
            characterRepository.characterList(0, new Callback<List<CharacterData>>() {
                @Override
                public void onSuccess(List<CharacterData> object) {
                    characterDatas = object;
                    //reverse order characets;
                    String comicId = "23123121";
                    comicRepository.comic(comicId, new Callback<ComicData>() {
                        @Override
                        public void onSuccess(ComicData comic) {
                            comicData = comic;
                            checkData();
                        }

                        @Override
                        public void onError(Throwable t) {
                            callback.onError(t);
                        }
                    });
                }

                @Override
                public void onError(Throwable t) {
                    callback.onError(t);
                }
            });

            String comicId = "1";
            comicRepository.comic(comicId, new Callback<ComicData>() {
                @Override
                public void onSuccess(ComicData comic) {
                    comicData = comic;
                    checkData();
                }

                @Override
                public void onError(Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }


    private void checkData() {
        if (characterDatas != null && comicData != null) {

            //unifico comicData con characterDatas
            // ...
            //Hago magia
            // ...
            //Invoco a Belcebú
            // ...
            //Devuelvo los datos.
            ComicMapper comicsMapper = new ComicMapper();
            callback.onSuccess(comicsMapper.toEntity(comicData));
        }
    }
}
