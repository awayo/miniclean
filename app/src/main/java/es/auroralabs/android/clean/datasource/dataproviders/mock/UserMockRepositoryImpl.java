package es.auroralabs.android.clean.datasource.dataproviders.mock;

import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by jmtm on 29/6/17.
 */

public class UserMockRepositoryImpl {

    public UserMockRepositoryImpl(Object[] configurator) {
        // Get configurator data here if needed
    }

    public void login(String email, String password, Callback<OAuthData> callback) {
        if (callback != null) {
            OAuthData oAuthData = new OAuthData();
            oAuthData.setAccessToken("access_token");
            oAuthData.setExpiresIn(10000);
            oAuthData.setRefreshToken("refresh_token");
            oAuthData.setTokenType("bearer");
            callback.onSuccess(oAuthData);
        }
    }

    public void signUp(String name, String lastName, String email, String phoneNumber, String password, String confirmPassword, Callback<OAuthData> callback) {
        if (callback != null) {
            OAuthData oAuthData = new OAuthData();
            oAuthData.setAccessToken("access_token");
            oAuthData.setExpiresIn(10000);
            oAuthData.setRefreshToken("refresh_token");
            oAuthData.setTokenType("bearer");
            callback.onSuccess(oAuthData);
        }
    }
}
