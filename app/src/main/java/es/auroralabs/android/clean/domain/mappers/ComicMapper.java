package es.auroralabs.android.clean.domain.mappers;

import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.tools.glifo.mappers.base.BasePropertiesMapper;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicMapper extends BasePropertiesMapper<ComicEntity, ComicData> {
    public ComicMapper() {
        super(ComicEntity.class, ComicData.class);
    }

    @Override
    public ComicData fromEntity(ComicEntity comicEntity) {
        ComicData comic = super.fromEntity(comicEntity);
        return comic;
    }

    @Override
    public ComicEntity toEntity(ComicData character) {
        ComicEntity comicEntity = super.toEntity(character);
        return comicEntity;
    }

    public List<ComicEntity> toEntity(List<ComicData> characters) {
        List<ComicEntity> list = new ArrayList<>();
        for (ComicData data : characters) {
            list.add(toEntity(data));
        }

        return list;
    }

    public List<ComicData> fromEntity(List<ComicEntity> characters) {

        List<ComicData> list = new ArrayList<>();
        for (ComicEntity data : characters) {
            list.add(fromEntity(data));
        }

        return list;
    }

}
