package es.auroralabs.android.clean.datasource.dataproviders.network.responses;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class CharacterWrapperResponse {
    CharactersDataResponse data;

    public CharactersDataResponse getData() {
        return data;
    }

    public void setData(CharactersDataResponse data) {
        this.data = data;
    }
}
