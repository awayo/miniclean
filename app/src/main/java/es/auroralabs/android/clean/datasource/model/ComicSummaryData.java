package es.auroralabs.android.clean.datasource.model;


import com.google.gson.annotations.SerializedName;

import es.auroralabs.tools.glifo.model.base.ModelData;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public class ComicSummaryData extends ModelData {
    String name;

    @SerializedName("resourceURI")
    String resourceURI;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }
}
