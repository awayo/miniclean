package es.auroralabs.android.clean.app.helpers;

import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import es.auroralabs.android.clean.R;

/**
 * Created by luismacb on 12/7/17.
 */

public class SnackbarHelper {

    public enum SnackbarStatus {
        SUCCESS,
        WARNING,
        ERROR,
        DEFAULT
    }

    public static class Builder {

        private Context context;
        private View view;
        private String message;
        private String actionText;
        private View.OnClickListener actionListener;
        private SnackbarStatus status;

        public Builder(Context context, View view) {
            this.context = context;
            this.view = view;

            // Set default values
            this.message = context.getString(R.string.common_server_error);
            this.actionText = context.getString(R.string.common_server_error_retry);
            this.status = SnackbarStatus.DEFAULT;
        }

        public Builder message(String message) {
            this.message = message;

            return this;
        }

        public Builder actionText (String actionText) {
            this.actionText = actionText;

            return this;
        }

        public Builder actionListener (View.OnClickListener listener) {
            this.actionListener = listener;

            return this;
        }

        public Builder status(SnackbarStatus status) {
            this.status = status;

            return this;
        }

        public TSnackbar build() {

            TSnackbar snackbar = TSnackbar.make(view, message, Snackbar.LENGTH_LONG);

            // Add action if needed
            if (actionListener != null) {
                snackbar.setAction(actionText, actionListener);
            }

            switch (status) {
                case ERROR: {
                    snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.snackbar_error_text));
                    snackbar.getView().setBackgroundColor(ContextCompat.getColor(context, R.color.snackbar_error_bg));
                    TextView textView = (TextView) snackbar.getView().findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(context, R.color.snackbar_error_text));
                }
                break;
                case SUCCESS:
                case WARNING:
                default: {
                    snackbar.setActionTextColor(ContextCompat.getColor(context, R.color.white));
                    TextView textView = (TextView) snackbar.getView().findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(ContextCompat.getColor(context, R.color.white));
                }
            }


            // Add padding due to the translucent status bar
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                float scale = context.getResources().getDisplayMetrics().density;
                int dpAsPixels = (int) (25 * scale + 0.5f);

                snackbar.getView().setPadding(0, dpAsPixels, 0, 0);
            }

            return snackbar;
        }
    }
}
