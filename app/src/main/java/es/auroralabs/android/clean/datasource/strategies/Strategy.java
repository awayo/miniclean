package es.auroralabs.android.clean.datasource.strategies;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public enum Strategy {
    MOCK,
    SERVICE_NO_CACHE,
    SERVICE_CACHE_FIRST,
}
