package es.auroralabs.android.clean.domain.repository;

import es.auroralabs.android.clean.datasource.model.OAuthData;

/**
 * Created by jmtm on 27/6/17.
 */

public interface PreferencesRepository extends Repository {

    boolean saveOAuthToken(OAuthData oauthData);

    boolean saveData(String data);

    String getData();
}
