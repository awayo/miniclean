package es.auroralabs.android.clean.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.Validator;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.usecases.users.LoginUserUseCase;

public class LoginActivity extends BaseActivity {

    private static final String LOG_TAG = LoginActivity.class.getName();

    @BindView(R.id.email_input_layout)
    TextInputLayout emailInputLayout;

    @BindView(R.id.email_text)
    EditText emailText;

    @BindView(R.id.password_input_layout)
    TextInputLayout passwordInputLayout;

    @BindView(R.id.password_text)
    EditText passwordText;

    @BindView(R.id.login_button)
    Button loginButton;

    @BindView(R.id.sign_up_button)
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        configureView();
    }

    private void configureView() {
        ButterKnife.bind(this);

        emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateEmail(emailText.getText().toString())) {
                    showValidationError(emailInputLayout, getString(R.string.login_email_validation_error));
                } else {

                    checkLoginButton();

                    emailInputLayout.setErrorEnabled(false);
                }
            }
        });
        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateEmail(s.toString())) {
                    emailInputLayout.setErrorEnabled(false);

                    checkLoginButton();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateRequiredString(passwordText.getText().toString())) {
                    showValidationError(passwordInputLayout, getString(R.string.login_password_validation_error));
                } else {

                    checkLoginButton();

                    passwordInputLayout.setErrorEnabled(false);
                }
            }
        });
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    tryLogin();

                    return true;
                }

                return false;
            }
        });
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    passwordInputLayout.setErrorEnabled(false);

                    checkLoginButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        loginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tryLogin();
            }
        });

        signUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                LoginActivity.this.startActivity(intent);
            }
        });
    }

    private void tryLogin() {
        hideKeyboard();

        if (validateForm()) {
            login();
        }

        checkLoginButton();
    }

    private void showMain() {
        overridePendingTransition(0, 0); // To disable animation when the activity finish due to the intent flags

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        LoginActivity.this.startActivity(intent);
    }

    private void login() {
        LoginUserUseCase loginUserUseCase = new LoginUserUseCase(this, Strategy.MOCK);
        loginUserUseCase.login(emailText.getText().toString(), passwordText.getText().toString(), new Callback<Boolean>() {
            @Override
            public void onSuccess(Boolean success) {
                if (success) {
                    showMain();
                } else {
                    showLoginError(getString(R.string.common_unknown_server_error));
                }
            }

            @Override
            public void onError(Throwable t) {
                showServerError();
            }
        });
    }

    private void checkLoginButton() {
        loginButton.setEnabled(!emailInputLayout.isErrorEnabled() && !passwordInputLayout.isErrorEnabled());
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showValidationError(TextInputLayout inputLayout, String errorMessage) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(errorMessage);

        // If there is an error, disable the button
        loginButton.setEnabled(false);
    }

    private boolean validateForm() {
        boolean errors = false;

        if (!Validator.validateEmail(emailText.getText().toString())) {
            showValidationError(emailInputLayout, getString(R.string.login_email_validation_error));

            errors = true;
        } else {
            emailInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validateRequiredString(passwordText.getText().toString())) {
            showValidationError(passwordInputLayout, getString(R.string.login_password_validation_error));

            errors = true;
        } else {
            passwordInputLayout.setErrorEnabled(false);
        }

        return !errors;
    }

    private void showServerError() {
        showServerError(new OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void showLoginError(String errorMessage) {

        hideKeyboard();

        showError(errorMessage);
    }
}

