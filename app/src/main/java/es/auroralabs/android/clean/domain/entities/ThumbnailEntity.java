package es.auroralabs.android.clean.domain.entities;

import android.os.Parcel;
import android.os.Parcelable;

import es.auroralabs.tools.glifo.entities.base.ModelEntity;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ThumbnailEntity extends ModelEntity implements Parcelable {
    String path;
    String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getURL() {
        return path+"."+extension;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.path);
        dest.writeString(this.extension);
    }

    public ThumbnailEntity() {
    }

    protected ThumbnailEntity(Parcel in) {
        this.path = in.readString();
        this.extension = in.readString();
    }

    public static final Parcelable.Creator<ThumbnailEntity> CREATOR = new Parcelable.Creator<ThumbnailEntity>() {
        @Override
        public ThumbnailEntity createFromParcel(Parcel source) {
            return new ThumbnailEntity(source);
        }

        @Override
        public ThumbnailEntity[] newArray(int size) {
            return new ThumbnailEntity[size];
        }
    };
}
