package es.auroralabs.android.clean.domain.callback;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public abstract class Callback<T> {
    public abstract void onSuccess(T object);

    public abstract void onError(Throwable t);
}
