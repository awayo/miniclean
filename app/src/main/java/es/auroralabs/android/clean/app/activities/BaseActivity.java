package es.auroralabs.android.clean.app.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.androidadvance.topsnackbar.TSnackbar;

import es.auroralabs.android.clean.app.helpers.SnackbarHelper;

/**
 * Created by franciscojosesotoportillo on 20/6/17.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // TODO: Add common code here
    }

    public void showError(String errorMessage) {

        TSnackbar snackbar = new SnackbarHelper.Builder(this, this.getWindow().getDecorView().getRootView())
                .message(errorMessage)
                .status(SnackbarHelper.SnackbarStatus.ERROR)
                .build();
        snackbar.show();

    }

    public void showSuccess(String message) {

        TSnackbar snackbar = new SnackbarHelper.Builder(this, this.getWindow().getDecorView().getRootView())
                .message(message)
                .status(SnackbarHelper.SnackbarStatus.SUCCESS)
                .build();
        snackbar.show();

    }

    public void showServerError(View.OnClickListener listener) {
        TSnackbar snackbar = new SnackbarHelper.Builder(this, this.getWindow().getDecorView().getRootView())
                .actionListener(listener)
                .build();
        snackbar.show();
    }
}
