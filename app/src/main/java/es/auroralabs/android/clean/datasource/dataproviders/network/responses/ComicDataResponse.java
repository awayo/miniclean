package es.auroralabs.android.clean.datasource.dataproviders.network.responses;

import java.util.List;

import es.auroralabs.android.clean.datasource.model.ComicData;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicDataResponse {
    List<ComicData> results;

    public List<ComicData> getResults() {
        return results;
    }

    public void setResults(List<ComicData> results) {
        this.results = results;
    }
}
