package es.auroralabs.android.clean.domain.entities;

import android.os.Parcel;
import android.os.Parcelable;

import es.auroralabs.tools.glifo.entities.base.ModelEntity;


/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicSummaryEntity extends ModelEntity implements Parcelable {
    String name;
    String resourceURI;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }


    @Override
    public int describeContents() {
        return this.name.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.resourceURI);
    }

    public ComicSummaryEntity() {
    }

    protected ComicSummaryEntity(Parcel in) {
        this.name = in.readString();
        this.resourceURI = in.readString();
    }

    public static final Parcelable.Creator<ComicSummaryEntity> CREATOR = new Parcelable.Creator<ComicSummaryEntity>() {
        @Override
        public ComicSummaryEntity createFromParcel(Parcel source) {
            return new ComicSummaryEntity(source);
        }

        @Override
        public ComicSummaryEntity[] newArray(int size) {
            return new ComicSummaryEntity[size];
        }
    };
}
