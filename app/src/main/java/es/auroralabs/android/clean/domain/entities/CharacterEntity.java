package es.auroralabs.android.clean.domain.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.tools.glifo.entities.base.ModelEntity;


/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class CharacterEntity extends ModelEntity implements Parcelable {

    public enum CharacterType {
        TYPE_ONE,
        TYPE_TWO,
        TYPE_THREE;

        public static CharacterType getCharacterType(String type) {
            if (!TextUtils.isEmpty(type)) {
                switch (type) {
                    case CharacterData.TYPE_ONE:
                        return TYPE_ONE;
                    case CharacterData.TYPE_TWO:
                        return TYPE_TWO;
                    case CharacterData.TYPE_THREE:
                        return TYPE_THREE;
                    default:
                        return null;
                }
            }

            return null;
        }
    }

    private String id;
    private String name;
    private ThumbnailEntity thumbnail;
    private List<ComicSummaryEntity> comics;
    private CharacterType type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ThumbnailEntity getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailEntity thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<ComicSummaryEntity> getComicSummary() {
        return comics;
    }

    public void setComicSummary(List<ComicSummaryEntity> comics) {
        this.comics = comics;
    }

    public List<ComicSummaryEntity> getComics() {
        return comics;
    }

    public void setComics(List<ComicSummaryEntity> comics) {
        this.comics = comics;
    }

    public CharacterType getType() {
        return type;
    }

    public void setType(CharacterType type) {
        this.type = type;
    }

    public CharacterEntity() {

    }

    @Override
    public String toString() {
        return getName() + " " + getId() + " " + getThumbnail();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeTypedList(this.comics);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }

    protected CharacterEntity(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.thumbnail = in.readParcelable(ThumbnailEntity.class.getClassLoader());
        this.comics = in.createTypedArrayList(ComicSummaryEntity.CREATOR);
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : CharacterType.values()[tmpType];
    }

    public static final Creator<CharacterEntity> CREATOR = new Creator<CharacterEntity>() {
        @Override
        public CharacterEntity createFromParcel(Parcel source) {
            return new CharacterEntity(source);
        }

        @Override
        public CharacterEntity[] newArray(int size) {
            return new CharacterEntity[size];
        }
    };
}
