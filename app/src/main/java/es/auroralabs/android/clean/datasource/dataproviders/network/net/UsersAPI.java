package es.auroralabs.android.clean.datasource.dataproviders.network.net;

import es.auroralabs.android.clean.datasource.dataproviders.network.requests.OAuthRefreshTokenRequest;
import es.auroralabs.android.clean.datasource.dataproviders.network.requests.OAuthTokenRequest;
import es.auroralabs.android.clean.datasource.dataproviders.network.requests.SignUpRequest;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by jmtm on 27/6/17.
 */

public interface UsersAPI {

    @POST("your_url")
    Call<OAuthData> token(@Body OAuthTokenRequest oAuth);

    @POST("your_url")
    Call<OAuthData> refreshToken(@Body OAuthRefreshTokenRequest oAuth);

    @POST("your_url")
    Call<OAuthData> signUp(@Body SignUpRequest oAuth);

}
