package es.auroralabs.android.clean.domain.usecases.comics;

import android.content.Context;

import es.auroralabs.android.clean.datasource.dataproviders.ComicDataProvider;
import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.android.clean.domain.mappers.ComicMapper;
import es.auroralabs.android.clean.domain.repository.ComicRepository;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class GetComicUseCase {

    private ComicRepository comicRepository;

    public GetComicUseCase(Context context, Strategy strategy, Object... configurator) {
        this.comicRepository = ComicDataProvider.getInstance(context, strategy, configurator);
    }

    public void retrieveComic(String comicId, final Callback<ComicEntity> callback) {

        if (callback != null) {
            comicRepository.comic(comicId, new Callback<ComicData>() {
                @Override
                public void onSuccess(ComicData comic) {
                    ComicMapper comicsMapper = new ComicMapper();
                    callback.onSuccess(comicsMapper.toEntity(comic));
                }

                @Override
                public void onError(Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }
}
