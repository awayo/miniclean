package es.auroralabs.android.clean.app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.android.clean.domain.usecases.comics.GetComicUseCase;

/**
 * Created by franciscojosesotoportillo on 4/1/17.
 */

public class ComicSummaryViewHolder extends RecyclerView.ViewHolder {

    ComicSummaryEntity comicSummaryEntity;

    TextView name;

    public ComicSummaryViewHolder(View itemView) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.name);

    }

    public void bind(final ComicSummaryEntity comicSummaryEntity) {
        this.comicSummaryEntity = comicSummaryEntity;
        name.setText(this.comicSummaryEntity.getName());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO
                GetComicUseCase getComicUseCase = new GetComicUseCase(itemView.getContext(), Strategy.SERVICE_CACHE_FIRST);

                String[] breakURL = comicSummaryEntity.getResourceURI().split("/");
                String id = breakURL[breakURL.length - 1];
                getComicUseCase.retrieveComic(id, new Callback<ComicEntity>() {
                    @Override
                    public void onSuccess(ComicEntity object) {
                        Toast.makeText(itemView.getContext(), object.getTitle(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(Throwable t) {

                    }
                });
            }
        });
    }
}
