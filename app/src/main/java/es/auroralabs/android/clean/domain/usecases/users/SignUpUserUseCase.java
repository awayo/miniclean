package es.auroralabs.android.clean.domain.usecases.users;

import android.content.Context;

import es.auroralabs.android.clean.datasource.dataproviders.PreferencesDataProvider;
import es.auroralabs.android.clean.datasource.dataproviders.UserDataProvider;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.repository.PreferencesRepository;
import es.auroralabs.android.clean.domain.repository.UserRepository;

/**
 * Created by jmtm on 29/6/17.
 */

public class SignUpUserUseCase {

    private UserRepository userRepository;
    private PreferencesRepository preferencesRepository;

    public SignUpUserUseCase(Context context, Strategy strategy, Object... configurator) {
        userRepository = UserDataProvider.getInstance(context, strategy, configurator);
        preferencesRepository = PreferencesDataProvider.getInstance(context);
    }

    public void signUp(String name, String lastName, String email, String phoneNumber, String password, String confirmPassword, final Callback<Boolean> callBack) {

        if (callBack != null) {
            userRepository.signUp(name, lastName, email, phoneNumber, password, confirmPassword, new Callback<OAuthData>() {
                @Override
                public void onSuccess(OAuthData oauthData) {
                    if (oauthData != null) {
                        callBack.onSuccess(preferencesRepository.saveOAuthToken(oauthData));
                    } else {
                        callBack.onSuccess(false);
                    }
                }

                @Override
                public void onError(Throwable t) {
                    callBack.onError(t);
                }
            });
        }
    }
}
