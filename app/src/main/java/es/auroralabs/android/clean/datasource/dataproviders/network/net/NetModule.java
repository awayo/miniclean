package es.auroralabs.android.clean.datasource.dataproviders.network.net;

import android.content.Context;
import android.content.Intent;
import android.os.ConditionVariable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import es.auroralabs.android.clean.BuildConfig;
import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.datasource.dataproviders.network.requests.OAuthRefreshTokenRequest;
import es.auroralabs.android.clean.datasource.dataproviders.preferences.Preferences;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import okhttp3.Authenticator;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public class NetModule {

    public static final String CLOSE_SESSION_INTENT_FILTER = "closeSessionIntentFilter";

    public enum ConverterType {
        JSON,
        XML
    }

    private static final ConditionVariable LOCK = new ConditionVariable(true);
    private static final AtomicBoolean isRefreshing = new AtomicBoolean(false);

    private static final String LOG_TAG = NetModule.class.getSimpleName();

    private String baseUrl;

    private Context context;
    private Retrofit retrofit;

    public NetModule(Context context) {
        this.context = context;
        this.baseUrl = BuildConfig.API_URL;
    }

    /**
     * Default constructor.
     *
     * @return Retrofit instance with a default GSON converter and a default OkHttpClient
     */
    public Retrofit provideRetrofit() {
        return provideRetrofit(provideConverterFactory(ConverterType.JSON), provideOkHttpClient());
    }

    public Retrofit provideRetrofit(ConverterType converterType) {
        return provideRetrofit(provideConverterFactory(converterType), provideOkHttpClient());
    }

    public Retrofit provideRetrofit(Converter.Factory converterFactory) {
        return provideRetrofit(converterFactory, provideOkHttpClient());
    }

    public Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return provideRetrofit(provideConverterFactory(ConverterType.JSON), okHttpClient);
    }

    public Retrofit provideRetrofit(Converter.Factory converterFactory, OkHttpClient okHttpClient) {
        retrofit = new Retrofit.Builder()
                .addConverterFactory(converterFactory)
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    private Converter.Factory provideConverterFactory(ConverterType converterType) {
        if (converterType != null) {
            switch (converterType) {
                case JSON:
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
                    return GsonConverterFactory.create(gsonBuilder.create());
                case XML:
                    return SimpleXmlConverterFactory.create();
                default:
                    break;
            }
        }

        return null;
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        // Authenticator for deal with OAuth
        //builder.authenticator(new SampleCleanAuthenticator());

        // Interceptor for add headers
        builder.addInterceptor(new SampleQueryCleanInterceptor());

        // Logging interceptor
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logginInterceptor = new HttpLoggingInterceptor();
            logginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logginInterceptor);
        }

        return builder.build();
    }

    /**
     * Sample authenticator to deal with oauth stuff
     */
    private class SampleCleanAuthenticator implements Authenticator {

        @Override
        public Request authenticate(Route route, okhttp3.Response response) throws IOException {

            /* Due to multiple asynch requests, we use an AtomicBoolean to indicate when the
            * token has been refreshing
            * */
            if (isRefreshing.compareAndSet(false, true)) {

                if (BuildConfig.DEBUG) {
                    Log.d(LOG_TAG, "Refreshing OAuth token");
                }

                // The first threat, locks the ConditionVariable until the token has been refreshed
                LOCK.close();

                // Refresh token
                OAuthData oAuthData = Preferences.getInstance(context).getOAuthToken();
                if (oAuthData != null && !TextUtils.isEmpty(oAuthData.getRefreshToken())) {
                    Call<OAuthData> refreshToken = retrofit.create(UsersAPI.class).refreshToken(new OAuthRefreshTokenRequest(oAuthData.getRefreshToken()));
                    retrofit2.Response<OAuthData> refreshResponse = refreshToken.execute();
                    if (refreshResponse.isSuccessful()) {
                        OAuthData newOAuthData = refreshResponse.body();
                        if (Preferences.getInstance(context).saveOAuthToken(newOAuthData)) {

                            // If the token has been refreshed successfully, it opens the ConditionVariable and changes the boolean flag
                            LOCK.open();
                            isRefreshing.set(false);

                            // Repeat the request with the correct credentials
                            return response.request().newBuilder().header("Authorization", "Bearer " + newOAuthData.getAccessToken()).build();

                        }
                    } else {
                        // Oauth error
                        if (BuildConfig.DEBUG) {
                            Log.e(LOG_TAG, "Error refreshing OAuth token: " + refreshResponse.message());
                        }

                        closeSession();
                    }
                }
            } else {
                // Oauth error
                if (!response.request().url().toString().contains("oauth/token")) {

                    if (BuildConfig.DEBUG) {
                        Log.d(LOG_TAG, "Waiting to OAuth token refreshed");
                    }

                    // If there are multiple requests, the last ones will be blocked until the oauth token has been refreshed
                    boolean conditionOpened = LOCK.block(10000);
                    // There, the oauth token has been refreshed (or timeout)
                    if (conditionOpened) {

                        if (BuildConfig.DEBUG) {
                            Log.d(LOG_TAG, "OAuth token refreshed");
                        }

                        // Repeat the request with the correct credentials
                        OAuthData newOauthData = Preferences.getInstance(context).getOAuthToken();
                        if (newOauthData != null && !TextUtils.isEmpty(newOauthData.getAccessToken())) {
                            // New send with correct credentials
                            return response.request().newBuilder().header("Authorization", "Bearer " + newOauthData.getAccessToken()).build();
                        }
                    }
                } else {

                    if (BuildConfig.DEBUG) {
                        Log.e(LOG_TAG, "Error refreshing OAuth token: " + response.message());
                    }

                    closeSession();
                }
            }

            return null;
        }
    }

    private void closeSession() {

        isRefreshing.set(false);

        if (context != null) {
            Preferences.getInstance(context).logout();

            // Send broadcast to close the session
            LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(CLOSE_SESSION_INTENT_FILTER));
        }
    }

    /**
     * Sample interceptor for add query parameters to the requests
     */
    private class SampleQueryCleanInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request newRequest;

            try {
                MessageDigest messageDigest = MessageDigest.getInstance("MD5");
                Date now = new Date();
                String ts = now.getTime() + "";
                String tokenString = ts + BuildConfig.WS_API_PRIVATE_KEY + BuildConfig.WS_API_PUBLIC_KEY;
                messageDigest.digest(tokenString.getBytes("UTF-8"));
                String hash = new BigInteger(1, messageDigest.digest(tokenString.getBytes("UTF-8"))).toString(16);
                HttpUrl url = request.url();
                url = url.newBuilder().addQueryParameter("ts", ts).addQueryParameter("hash", hash).addQueryParameter("apikey", BuildConfig.WS_API_PUBLIC_KEY).build();
                newRequest = request.newBuilder()
                        .url(url)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
                return chain.proceed(request);
            }

            return chain.proceed(newRequest);
        }
    }

    /**
     * Sample interceptor for add header parameters to the requests
     */
    private class SampleHeaderCleanInterceptor implements Interceptor {

        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept-Language", context.getString(R.string.language_code));

            // Add authorization token if exists
            OAuthData oAuthData = Preferences.getInstance(context).getOAuthToken();
            if (oAuthData != null && !TextUtils.isEmpty(oAuthData.getAccessToken())) {
                builder.addHeader("Authorization", "Bearer " + oAuthData.getAccessToken());
            }

            return chain.proceed(builder.build());
        }
    }
}
