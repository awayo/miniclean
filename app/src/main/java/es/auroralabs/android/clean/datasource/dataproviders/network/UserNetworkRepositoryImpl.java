package es.auroralabs.android.clean.datasource.dataproviders.network;

import android.content.Context;

import es.auroralabs.android.clean.datasource.dataproviders.network.net.NetModule;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.UsersAPI;
import es.auroralabs.android.clean.datasource.dataproviders.network.requests.OAuthTokenRequest;
import es.auroralabs.android.clean.datasource.dataproviders.network.requests.SignUpRequest;
import es.auroralabs.android.clean.datasource.model.OAuthData;
import es.auroralabs.android.clean.domain.callback.Callback;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by jmtm on 29/6/17.
 */

public class UserNetworkRepositoryImpl {

    private Retrofit retrofit;

    public UserNetworkRepositoryImpl(Context context, Object... configurator) {
        // Get configurator data here if needed
        NetModule netModule = new NetModule(context);
        retrofit = netModule.provideRetrofit();
    }

    public void login(String email, String password, final Callback<OAuthData> callback) {
        if (callback != null) {
            Call<OAuthData> login = retrofit.create(UsersAPI.class).token(new OAuthTokenRequest(email, password));
            login.enqueue(new retrofit2.Callback<OAuthData>() {
                @Override
                public void onResponse(Call<OAuthData> call, Response<OAuthData> response) {
                    if (response.isSuccessful()) {

                        callback.onSuccess(response.body());

                    } else {
                        callback.onError(new Exception());
                    }
                }

                @Override
                public void onFailure(Call<OAuthData> call, Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }

    public void signUp(String name, String lastName, String email, String phoneNumber, String password, String confirmPassword, final Callback<OAuthData> callback) {
        if (callback != null) {
            Call<OAuthData> signUp = retrofit.create(UsersAPI.class).signUp(new SignUpRequest(name, lastName, email, phoneNumber, password, confirmPassword));
            signUp.enqueue(new retrofit2.Callback<OAuthData>() {
                @Override
                public void onResponse(Call<OAuthData> call, Response<OAuthData> response) {
                    if (response.isSuccessful()) {

                        callback.onSuccess(response.body());

                    } else {
                        callback.onError(new Exception());
                    }
                }

                @Override
                public void onFailure(Call<OAuthData> call, Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }
}
