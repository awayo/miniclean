package es.auroralabs.android.clean.datasource.dataproviders.network;

import android.content.Context;

import java.io.IOException;
import java.util.List;

import es.auroralabs.android.clean.datasource.cache.Cache;
import es.auroralabs.android.clean.datasource.cache.SimpleMemoryCache;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.MarvelAPI;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.NetModule;
import es.auroralabs.android.clean.datasource.dataproviders.network.responses.CharacterWrapperResponse;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

//stub this one can be disk access, cloud access, SimpleQL, etc etc
public class CharacterNetworkRepositoryImpl implements CharacterRepository {

    private int limit = 10;
    private Retrofit retrofit;
    private Cache<CharacterWrapperResponse> cache;

    public CharacterNetworkRepositoryImpl(Context context, boolean cache, Object... configurator) {
        // Get configurator data here if needed
        NetModule netModule = new NetModule(context);
        retrofit = netModule.provideRetrofit();
        if (cache) {
            this.cache = new SimpleMemoryCache<>();
        }
    }

    public void characterList(Integer page, final es.auroralabs.android.clean.domain.callback.Callback<List<CharacterData>> callback) {
        if (callback != null) {
            // FIXME: 4/1/18 - Add cache
            Call<CharacterWrapperResponse> characters = retrofit.create(MarvelAPI.class).getCharacters(limit, limit * page);
            characters.enqueue(new Callback<CharacterWrapperResponse>() {
                @Override
                public void onResponse(Call<CharacterWrapperResponse> call, retrofit2.Response<CharacterWrapperResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body().getData().getResults());
                    } else {
                        String error = "";
                        try {
                            error = response.errorBody().string();
                        } catch (IOException ioe) {
                        }

                        callback.onError(new Throwable(error));
                    }
                }

                @Override
                public void onFailure(Call<CharacterWrapperResponse> call, Throwable t) {
                    callback.onError(t);
                }
            });
        }

    }

    @Override
    public void character(final String characterId, final es.auroralabs.android.clean.domain.callback.Callback<CharacterData> callback) {
        if (callback != null) {
            CharacterWrapperResponse response = null;
            if (cache != null) {
                if (cache.get(characterId) != null) {
                    response = cache.get(characterId);
                    callback.onSuccess(response.getData().getResults().get(0));
                }
            }
            // FIXME: 4/1/18 - Change cache strategy. Now it there are data in the cache, it doesn't call the service
            if (response == null) {
                Call<CharacterWrapperResponse> character = retrofit.create(MarvelAPI.class).getCharacter(characterId);
                character.enqueue(new Callback<CharacterWrapperResponse>() {
                    @Override
                    public void onResponse(Call<CharacterWrapperResponse> call, retrofit2.Response<CharacterWrapperResponse> response) {
                        if (response.isSuccessful()) {
                            CharacterData characterData = response.body().getData().getResults().get(0);
                            if (cache != null) {
                                cache.add(characterId, response.body());
                            }
                            callback.onSuccess(characterData);
                        } else {
                            String error = "";
                            try {
                                error = response.errorBody().string();
                            } catch (IOException ioe) {
                            }

                            callback.onError(new Throwable(error));
                        }
                    }

                    @Override
                    public void onFailure(Call<CharacterWrapperResponse> call, Throwable t) {
                        callback.onError(t);
                    }
                });
            }
        }
    }

}
