package es.auroralabs.android.clean.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.app.viewholders.CharacterViewHolder;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class CharactersAdapter extends RecyclerView.Adapter<CharacterViewHolder> {

    List<CharacterEntity> list;

    public CharactersAdapter(List<CharacterEntity> list) {
        this.list = list;
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.character_cell, parent, false);
        return new CharacterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}
