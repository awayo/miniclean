package es.auroralabs.android.clean.datasource.dataproviders.network.responses;

import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class CharactersDataResponse {
    List<CharacterData> results;

    public List<CharacterData> getResults() {
        return results;
    }

    public void setResults(List<CharacterData> results) {
        this.results = results;
    }
}
