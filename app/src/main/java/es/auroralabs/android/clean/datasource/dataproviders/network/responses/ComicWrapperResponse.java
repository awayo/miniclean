package es.auroralabs.android.clean.datasource.dataproviders.network.responses;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class ComicWrapperResponse {
    ComicDataResponse data;

    public ComicDataResponse getData() {
        return data;
    }

    public void setData(ComicDataResponse data) {
        this.data = data;
    }
}
