package es.auroralabs.android.clean.app.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.app.adapters.ComicsAdapter;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.android.clean.domain.usecases.characters.GetCharacterUseCase;

import static es.auroralabs.android.clean.app.viewholders.CharacterViewHolder.drawableToBitmap;


public class CharacterDetailActivity extends AppCompatActivity {

    public static final String CHARACTER_ID_BUNDLE = "characterId";

    private CharacterEntity characterEntity;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.thumbnail)
    ImageView thumbnail;
    @BindView(R.id.comics)
    RecyclerView comicsReciclerView;

    ComicsAdapter comicsAdapter;
    List<ComicSummaryEntity> comicSummaryEntityList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_detail);

        configureScreen();

        fillScreen();

        GetCharacterUseCase useCaseGetCharacter = new GetCharacterUseCase(this, Strategy.SERVICE_CACHE_FIRST);
        useCaseGetCharacter.retrieveCharacter(characterEntity.getId(), new Callback<CharacterEntity>() {
            @Override
            public void onSuccess(CharacterEntity object) {
                characterEntity = object;
                fillScreen();
            }

            @Override
            public void onError(Throwable t) {
                Toast.makeText(CharacterDetailActivity.this, "Error - " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void configureScreen() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        if (getIntent().hasExtra(CHARACTER_ID_BUNDLE)) {
            characterEntity = (CharacterEntity) getIntent().getExtras().get(CHARACTER_ID_BUNDLE);
        } else {
            finish();
        }

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        comicsReciclerView.setLayoutManager(linearLayoutManager);

        comicSummaryEntityList = characterEntity.getComicSummary();

        comicsAdapter = new ComicsAdapter(comicSummaryEntityList);

        comicsReciclerView.setAdapter(comicsAdapter);


    }

    private void fillScreen() {
        name.setText(this.characterEntity.getName());

        Glide.with(this).load(this.characterEntity.getThumbnail().getURL())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        Palette palette = Palette.from(drawableToBitmap(resource)).generate();
                        int defaultColor = ContextCompat.getColor(CharacterDetailActivity.this, R.color.colorPrimary);
                        name.setTextColor(palette.getLightVibrantColor(defaultColor));
                        findViewById(R.id.content_character_detail).setBackgroundColor(palette.getDarkMutedColor(defaultColor));
                        return false;
                    }

                })
                .into(thumbnail);
    }

}
