package es.auroralabs.android.clean.domain.usecases.characters;

import android.content.Context;

import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.datasource.dataproviders.CharacterDataProvider;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.mappers.CharacterMapper;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public class GetCharacterUseCase {

    CharacterRepository characterRepository;

    public GetCharacterUseCase(Context context, Strategy strategy) {
        this.characterRepository = CharacterDataProvider.getInstance(context, strategy);
    }

    public void retrieveCharacter(String characterId, final Callback<CharacterEntity> callback) {

        if (callback != null) {
            characterRepository.character(characterId, new Callback<CharacterData>() {
                @Override
                public void onSuccess(CharacterData character) {
                    CharacterMapper charactersMapper = new CharacterMapper();
                    callback.onSuccess(charactersMapper.toEntity(character));
                }

                @Override
                public void onError(Throwable t) {
                    callback.onError(t);
                }
            });
        }
    }
}
