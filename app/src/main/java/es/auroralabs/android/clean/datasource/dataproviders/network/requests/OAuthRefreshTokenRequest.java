package es.auroralabs.android.clean.datasource.dataproviders.network.requests;

import es.auroralabs.android.clean.BuildConfig;

/**
 * Created by jmtm on 27/6/17.
 */

public class OAuthRefreshTokenRequest {

    private static final String GRANT_TYPE = "refresh_token";

    private String grantType = GRANT_TYPE;
    private String clientId = BuildConfig.API_CLIENT_ID;
    private String clientSecret = BuildConfig.API_CLIENT_SECRET;
    private String refreshToken;

    public OAuthRefreshTokenRequest(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getGrantType() {
        return grantType;
    }

    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
