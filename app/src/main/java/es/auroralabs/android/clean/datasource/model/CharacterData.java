package es.auroralabs.android.clean.datasource.model;


import es.auroralabs.tools.glifo.model.base.ModelData;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

public class CharacterData extends ModelData {

    public static final String TYPE_ONE = "1";
    public static final String TYPE_TWO = "2";
    public static final String TYPE_THREE = "3";

    String id;
    String name;
    ThumbnailData thumbnail;
    ComicItemsData comics;
    String type;

    public ComicItemsData getComicSummary() {
        return comics;
    }

    public void setComicSummary(ComicItemsData comics) {
        this.comics = comics;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ThumbnailData getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailData thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ComicItemsData getComics() {
        return comics;
    }

    public void setComics(ComicItemsData comics) {
        this.comics = comics;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
