package es.auroralabs.android.clean.datasource.dataproviders.preferences;

import android.content.Context;

import es.auroralabs.android.clean.datasource.model.OAuthData;

/**
 * Created by jmtm on 27/6/17.
 */

public class PreferencesRepositoryImpl {

    private Preferences preferences;

    public PreferencesRepositoryImpl(Context context) {
        preferences = Preferences.getInstance(context);
    }

    public boolean saveOAuthToken(OAuthData oauthData) {
        return preferences.saveOAuthToken(oauthData);
    }

    public boolean saveData(String data) {
        return preferences.saveData(data);
    }

    public String getData() {
        return preferences.getData();
    }
}
