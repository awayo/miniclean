package es.auroralabs.android.clean.domain.repository;

import java.util.List;

import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public interface CharacterRepository extends Repository{

    void characterList(Integer page, Callback<List<CharacterData>> characters);
    void character(String characterId, Callback<CharacterData> character);

}
