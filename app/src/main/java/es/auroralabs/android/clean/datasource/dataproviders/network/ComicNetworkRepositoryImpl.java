package es.auroralabs.android.clean.datasource.dataproviders.network;

import android.content.Context;

import java.io.IOException;

import es.auroralabs.android.clean.datasource.cache.Cache;
import es.auroralabs.android.clean.datasource.cache.SimpleMemoryCache;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.MarvelAPI;
import es.auroralabs.android.clean.datasource.dataproviders.network.net.NetModule;
import es.auroralabs.android.clean.datasource.dataproviders.network.responses.ComicWrapperResponse;
import es.auroralabs.android.clean.datasource.model.ComicData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */

//stub this one can be disk access, cloud access, SimpleQL, etc etc
public class ComicNetworkRepositoryImpl {

    Retrofit retrofit;
    Cache<ComicWrapperResponse> cache;

    public ComicNetworkRepositoryImpl(Context context, boolean cache, Object... configurator) {
        // Get configurator data here if needed
        NetModule netModule = new NetModule(context);
        retrofit = netModule.provideRetrofit();
        if (cache) {
            this.cache = new SimpleMemoryCache<>();
        }
    }

    public void comic(final String comicId, final es.auroralabs.android.clean.domain.callback.Callback<ComicData> callback) {
        if (callback != null) {
            ComicWrapperResponse response = null;
            if (cache != null) {
                if (cache.get(comicId) != null) {
                    response = cache.get(comicId);
                    callback.onSuccess(response.getData().getResults().get(0));
                }
            }
            // FIXME: 4/1/18 - Change cache strategy. Now it there are data in the cache, it doesn't call the service
            if (response == null) {
                Call<ComicWrapperResponse> comic = retrofit.create(MarvelAPI.class).getComic(comicId);
                comic.enqueue(new Callback<ComicWrapperResponse>() {
                    @Override
                    public void onResponse(Call<ComicWrapperResponse> call, retrofit2.Response<ComicWrapperResponse> response) {
                        if (response.isSuccessful()) {
                            ComicData characterData = response.body().getData().getResults().get(0);
                            if (cache != null) {
                                cache.add(comicId, response.body());
                            }
                            callback.onSuccess(characterData);
                        } else {
                            String error = "";
                            try {
                                error = response.errorBody().string();
                            } catch (IOException ioe) {
                            }

                            callback.onError(new Throwable(error));
                        }
                    }

                    @Override
                    public void onFailure(Call<ComicWrapperResponse> call, Throwable t) {
                        callback.onError(t);
                    }
                });

            }

        }
    }

}
