package es.auroralabs.android.clean.app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.auroralabs.android.clean.R;
import es.auroralabs.android.clean.app.helpers.SnackbarHelper;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.Validator;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.usecases.users.SignUpUserUseCase;

public class SignUpActivity extends BaseActivity {

    private static final String LOG_TAG = SignUpActivity.class.getName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.user_name_input_layout)
    TextInputLayout userNameInputLayout;

    @BindView(R.id.user_name_text)
    EditText userNameText;

    @BindView(R.id.user_last_name_input_layout)
    TextInputLayout userLastNameInputLayout;

    @BindView(R.id.user_last_name_text)
    EditText userLastNameText;

    @BindView(R.id.email_input_layout)
    TextInputLayout emailInputLayout;

    @BindView(R.id.email_text)
    EditText emailText;

    @BindView(R.id.phone_number_input_layout)
    TextInputLayout phoneNumberInputLayout;

    @BindView(R.id.phone_number_text)
    EditText phoneNumberText;

    @BindView(R.id.password_input_layout)
    TextInputLayout passwordInputLayout;

    @BindView(R.id.password_text)
    EditText passwordText;

    @BindView(R.id.repeat_password_input_layout)
    TextInputLayout repeatPasswordInputLayout;

    @BindView(R.id.repeat_password_text)
    EditText repeatPasswordText;

    @BindView(R.id.sign_up_button)
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        configureView();
    }

    private void configureView() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        userNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateRequiredString(userNameText.getText().toString())) {
                    showValidationError(userNameInputLayout, getString(R.string.sign_up_first_name_validation_error));
                } else {

                    checkSignUpButton();

                    userNameInputLayout.setErrorEnabled(false);
                }
            }
        });
        userNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    userNameInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        userLastNameText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateRequiredString(userLastNameText.getText().toString())) {
                    showValidationError(userLastNameInputLayout, getString(R.string.sign_up_last_name_validation_error));
                } else {

                    checkSignUpButton();

                    userLastNameInputLayout.setErrorEnabled(false);
                }
            }
        });
        userLastNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    userLastNameInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateEmail(emailText.getText().toString())) {
                    showValidationError(emailInputLayout, getString(R.string.sign_up_email_validation_error));
                } else {

                    checkSignUpButton();

                    emailInputLayout.setErrorEnabled(false);
                }
            }
        });
        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateEmail(s.toString())) {
                    emailInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        phoneNumberText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validateRequiredString(phoneNumberText.getText().toString())) {
                    showValidationError(phoneNumberInputLayout, getString(R.string.sign_up_phone_number_validation_error));
                } else {

                    checkSignUpButton();

                    phoneNumberInputLayout.setErrorEnabled(false);
                }
            }
        });
        phoneNumberText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    phoneNumberInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus && !Validator.validatePassword(passwordText.getText().toString())) {
                    showValidationError(passwordInputLayout, getString(R.string.sign_up_password_validation_error));
                } else {

                    checkSignUpButton();

                    passwordInputLayout.setErrorEnabled(false);
                }
            }
        });
        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    passwordInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        repeatPasswordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // Validate content
                if (!hasFocus) {
                    if (!Validator.validatePassword(repeatPasswordText.getText().toString())) {
                        showValidationError(repeatPasswordInputLayout, getString(R.string.sign_up_password_validation_error));
                    } else if (!Validator.validateConfirmPassword(passwordText.getText().toString(), repeatPasswordText.getText().toString())) {
                        showValidationError(repeatPasswordInputLayout, getString(R.string.sign_up_repeat_password_validation_error));
                    }
                } else {

                    checkSignUpButton();

                    repeatPasswordInputLayout.setErrorEnabled(false);
                }
            }
        });
        repeatPasswordText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {

                    trySignUp();

                    return true;
                }

                return false;
            }
        });
        repeatPasswordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Validator.validateRequiredString(s.toString())) {
                    repeatPasswordInputLayout.setErrorEnabled(false);

                    checkSignUpButton();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to do
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                trySignUp();
            }
        });
    }

    private void checkSignUpButton() {
        signUpButton.setEnabled(!userNameInputLayout.isErrorEnabled() && !userLastNameInputLayout.isErrorEnabled() && !emailInputLayout.isErrorEnabled()
                && !phoneNumberInputLayout.isErrorEnabled() && !passwordInputLayout.isErrorEnabled() && !repeatPasswordInputLayout.isErrorEnabled());
    }

    private void trySignUp() {
        hideKeyboard();

        if (validateForm()) {
            signUp();
        }

        checkSignUpButton();
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showValidationError(TextInputLayout inputLayout, String errorMessage) {
        inputLayout.setErrorEnabled(true);
        inputLayout.setError(errorMessage);

        // If there is an error, disable the button
        signUpButton.setEnabled(false);

    }

    private boolean validateForm() {
        boolean errors = false;

        if (!Validator.validateRequiredString(userNameText.getText().toString())) {
            showValidationError(userNameInputLayout, getString(R.string.sign_up_first_name_validation_error));

            errors = true;
        } else {
            userNameInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validateRequiredString(userLastNameText.getText().toString())) {
            showValidationError(userLastNameInputLayout, getString(R.string.sign_up_last_name_validation_error));

            errors = true;
        } else {
            userLastNameInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validateEmail(emailText.getText().toString())) {
            showValidationError(emailInputLayout, getString(R.string.sign_up_email_validation_error));

            errors = true;
        } else {
            emailInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validateRequiredString(phoneNumberText.getText().toString())) {
            showValidationError(phoneNumberInputLayout, getString(R.string.sign_up_phone_number_validation_error));

            errors = true;
        } else {
            phoneNumberInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validatePassword(passwordText.getText().toString())) {
            showValidationError(passwordInputLayout, getString(R.string.sign_up_password_validation_error));

            errors = true;
        } else {
            passwordInputLayout.setErrorEnabled(false);
        }

        if (!Validator.validateConfirmPassword(passwordText.getText().toString(), repeatPasswordText.getText().toString())) {
            showValidationError(repeatPasswordInputLayout, getString(R.string.sign_up_repeat_password_validation_error));

            errors = true;
        } else {
            repeatPasswordInputLayout.setErrorEnabled(false);
        }

        return !errors;
    }

    private void signUp() {
        SignUpUserUseCase signUpUserUseCase = new SignUpUserUseCase(this, Strategy.SERVICE_NO_CACHE);
        signUpUserUseCase.signUp(userNameText.getText().toString(), userLastNameText.getText().toString(), emailText.getText().toString(), phoneNumberText.getText().toString(), passwordText.getText().toString(), repeatPasswordText.getText().toString(), new Callback<Boolean>() {
            @Override
            public void onSuccess(Boolean success) {
                if (success) {
                    showMain();
                } else {
                    showServerError();
                }
            }

            @Override
            public void onError(Throwable t) {
                // Show error message
                showSignUpError(getString(R.string.common_server_error));
            }
        });
    }

    private void showMain() {
        overridePendingTransition(0, 0); // To disable animation when the activity finish due to the intent flags

        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        SignUpActivity.this.startActivity(intent);
    }

    private void showSignUpError(String errorMessage) {

        hideKeyboard();

        TSnackbar snackbar = new SnackbarHelper.Builder(this, signUpButton)
                .message(errorMessage)
                .status(SnackbarHelper.SnackbarStatus.ERROR).build();
        snackbar.show();
    }

    private void showServerError() {

        hideKeyboard();

        showServerError(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();

            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }
}