package es.auroralabs.android.clean.domain.repository;

import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.domain.callback.Callback;

/**
 * Created by franciscojosesotoportillo on 3/1/17.
 */

public interface ComicRepository extends Repository{

    void comic(String comicId, Callback<ComicData> comic);

}
