package es.auroralabs.android.clean.datasource;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import es.auroralabs.android.clean.datasource.model.ComicData;
import es.auroralabs.android.clean.datasource.model.ThumbnailData;
import es.auroralabs.android.clean.domain.entities.ComicEntity;
import es.auroralabs.android.clean.domain.entities.ThumbnailEntity;
import es.auroralabs.android.clean.domain.mappers.ComicMapper;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */
@RunWith(AndroidJUnit4.class)
public class DatasourceComicsTest {

    @Test
    public void mapperFromEntity_Success() {

        ComicEntity entity = new ComicEntity();
        entity.setDescription("1212");
        entity.setTitle("12asdasdasd12");
        ThumbnailEntity thumbnailEntity = new ThumbnailEntity();
        thumbnailEntity.setPath("asssss");
        entity.setThumbnail(thumbnailEntity);

        ComicMapper comicMapper = new ComicMapper();
        ComicData data = comicMapper.fromEntity(entity);

        assertNotNull(data);
    }

    @Test
    public void mapperToEntity_Success() {

        ComicData entity = new ComicData();
        entity.setDescription("1212");
        entity.setTitle("12asdasdasd12");
        entity.setIssueNumber(2);
        entity.setPageCount(21);
        ThumbnailData thumbnailEntity = new ThumbnailData();
        thumbnailEntity.setPath("asssss");
        entity.setThumbnail(thumbnailEntity);

        ComicMapper comicMapper = new ComicMapper();
        ComicEntity data = comicMapper.toEntity(entity);

        assertNotNull(data);
    }

}
