package es.auroralabs.android.clean.datasource;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.util.concurrent.SettableFuture;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import es.auroralabs.android.clean.datasource.dataproviders.CharacterDataProvider;
import es.auroralabs.android.clean.datasource.model.CharacterData;
import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.entities.ComicSummaryEntity;
import es.auroralabs.android.clean.domain.entities.ThumbnailEntity;
import es.auroralabs.android.clean.domain.mappers.CharacterMapper;
import es.auroralabs.android.clean.domain.repository.CharacterRepository;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */
@RunWith(AndroidJUnit4.class)
public class DatasourceCharactersTest {

    private Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void mapperFromEntity_Success() {

        CharacterEntity characterData = new CharacterEntity();
        characterData.setId("1212");
        characterData.setName("12asdasdasd12");
        characterData.setComicSummary(new ArrayList<ComicSummaryEntity>());
        ThumbnailEntity thumbnailEntity = new ThumbnailEntity();
        thumbnailEntity.setPath("asssss");
        characterData.setThumbnail(thumbnailEntity);

        CharacterMapper characterMapper = new CharacterMapper();
        CharacterData data = characterMapper.fromEntity(characterData);

        assertNotNull(data);
    }

    @Test
    public void getCharacters_ReturnsList() {

        CharacterRepository characterRepository = CharacterDataProvider.getInstance(context, Strategy.SERVICE_NO_CACHE);
        final SettableFuture<List<CharacterData>> future = SettableFuture.create();
        characterRepository.characterList(0, new Callback<List<CharacterData>>() {
            @Override
            public void onSuccess(List<CharacterData> object) {
                future.set(object);
            }

            @Override
            public void onError(Throwable t) {
                Assert.assertNull(t);
            }
        });

        try {
            List<CharacterData> object = future.get();
            Assert.assertNotNull(object);
            Assert.assertTrue(object.size() == 10);
            Assert.assertTrue(object.get(0) instanceof CharacterData);
            Assert.assertNotNull(object.get(0).getId());

        } catch (InterruptedException ie) {
            Assert.assertTrue("Error", false);
        } catch (ExecutionException ee) {
            Assert.assertTrue("Error", false);
        }
    }

    @Test
    public void getCharacter_ReturnsOne() {

        CharacterRepository characterRepository = CharacterDataProvider.getInstance(context, Strategy.SERVICE_NO_CACHE);
        final SettableFuture<CharacterData> future = SettableFuture.create();
        characterRepository.character("1011334", new Callback<CharacterData>() {
            @Override
            public void onSuccess(CharacterData object) {
                future.set(object);
            }

            @Override
            public void onError(Throwable t) {
                Assert.assertNull(t);
            }
        });

        try {
            CharacterData object = future.get();
            Assert.assertNotNull(object);
            Assert.assertTrue(object instanceof CharacterData);
        } catch (InterruptedException ie) {
            Assert.assertTrue("Error", false);
        } catch (ExecutionException ee) {
            Assert.assertTrue("Error", false);
        }
    }
}
