package es.auroralabs.android.clean.domain;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.util.concurrent.SettableFuture;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.ExecutionException;

import es.auroralabs.android.clean.datasource.strategies.Strategy;
import es.auroralabs.android.clean.domain.callback.Callback;
import es.auroralabs.android.clean.domain.entities.CharacterEntity;
import es.auroralabs.android.clean.domain.usecases.characters.GetCharactersUseCase;

/**
 * Created by franciscojosesotoportillo on 2/1/17.
 */
@RunWith(AndroidJUnit4.class)
public class DomainCharactersTest {

    private Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void getCharacters_ReturnsList() {

        GetCharactersUseCase getCharactersUseCase = new GetCharactersUseCase(context, Strategy.SERVICE_NO_CACHE);
        final SettableFuture<List<CharacterEntity>> future = SettableFuture.create();
        getCharactersUseCase.retrieveCharacterList(0, new Callback<List<CharacterEntity>>() {
            @Override
            public void onSuccess(List<CharacterEntity> object) {
                future.set(object);
            }

            @Override
            public void onError(Throwable t) {
                Assert.assertNull(t);
            }
        });

        try {
            Assert.assertNotNull(future.get());
            Assert.assertTrue(future.get().size() == 10);
            Assert.assertTrue(future.get().get(0) instanceof CharacterEntity);
            Assert.assertNotNull(future.get().get(0).getId());
        } catch (InterruptedException ie) {
            Assert.assertTrue("Error", false);
        } catch (ExecutionException ee) {
            Assert.assertTrue("Error", false);
        }
    }
}
