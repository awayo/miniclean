# Sample project #

Aplicación en Android donde se irán incluyendo los últimos desarrollos realizados en proyectos de clientes que puedan ser reutilizados en futuros proyectos. 

Además, sirve de ejemplo de cómo organizar una aplicación Android usando **Clean Architecture**.

## Indice ##

- [Requisitos](#markdown-requisitos)
- [Localizables](#markdown-localizables)
- [Librerías](#markdown-librerías)
- [Características específicas](#markdown-caracteristicas-especificas)
	- [README](#markdown-readme-template)
	- [Preferencias](#markdown-preferencias)
	- [Autenticación OAuth](#markdown-autenticación-oauth)
	- [Login](#markdown-login)
	- [Botones (Ripple)](#markdown-botones-(ripple))

## Requisitos ##

* Android 4.4(19) o superior
* Andriod Studio 3.0.1 o superior

### Librerías ###

* Retrofit 2
* Butterknife
* Glide

### Localizables ###

Para generar los localizables, se hace uso de la librería [Localio](https://github.com/mrmans0n/localio). 

El documento para la generación de los textos localizados se encuentra en el siguiente enlace: [Localizables](https://docs.google.com/spreadsheets/d/1Xt2LUlyWnyq35cwfzv-lVPho5qdXEraYReqJq5McydM/edit#gid=0)

Para generar los localizables habrá que ejecutar ejecutar el comando `localize`

### Características específicas ###

A continuación se detallan las características específicas del proyecto a tener en cuenta para futuros desarrollos. 

#### README Template ####

En el proyecto existe un [README Template](./README_TEMPLATE.md) para poder ser reutilizado en futuros proyectos. En este fichero existen una serie de secciones predeterminadas que habrá que ir completando (o eliminando si no aplica) para incliur la información específica del proyecto que se crea oportuna. 

#### Preferencias ####

Se incluyen las clases necesarias para el acceso a las preferencias de usuario, ya sea a nivel de aplicación (o dominio) como a nivel de datasource. 

#### Autenticación OAuth ####

Se incluye un ejemplo de cómo hacer llamadas a una API bajo autenticación OAuth.

#### Login ####

Se incluye una vista de ejemplo de login con las siguientes características a destacar:

* Validación inline de los campos: Se incluyen validadores para el caso del email y el password. Los errores son mostrados de forma inline sobre los propios campos.
* Errores del servidor: Se muestran los distintos casos de errores recogidos desde el servidor como avisos en la parte superior de la vista.

Es posible hacer login con cualquier dato que pase las validaciones existentes para el email y el password.

#### Registro ####

Se incluye una vista de ejemplo para registro de usuario las siguientes características a destacar:

* Validación inline de los campos: Se incluyen validadores para todos los campos del formulario. Los errores son mostrados de forma inline sobre los propios campos.
* Errores del servidor: Se muestran los distintos casos de errores recogidos desde el servidor como avisos en la parte superior de la vista.

Es posible registrar a un usuario con cualquier dato que pase las validaciones existentes para los campos. 

#### Botones (ripple) ####

Se incluyen ejemplos de drawables para los botones en los que se incluye el efecto ripple con drawables válidos para cualquier versión de android. 




