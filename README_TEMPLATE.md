# Título del proyecto #

Breve descripción del proyecto

## Indice ##

- [Requisitos](#markdown-requisitos)
- [Localizables](#markdown-localizables)
- [Librerías](#markdown-librerías)
- [Mapas](#markdown-mapas)
- [Entornos](#markdown-entornos)
- [Usuarios](#markdown-usuarios)
- [Integración continua](#markdown-integracion-continua)
- [Certificados](#markdown-certificados)
- [Entregas al cliente](#markdown-entregas-al-cliente)
- [Características específicas](#markdown-caracteristicas-especificas)

## Requisitos

> Completa los requisitos del proyecto

- Android <set_version>
- Android Studio <set_version>

### Localizables ###

Para generar los localizables, se hace uso de la librería [Localio](https://github.com/mrmans0n/localio). 

> No olvides actualizar la url de los localizables

El documento para la generación de los textos localizados se encuentra en el siguiente enlace: [Localizables](https://set_your_url)

Para generar los localizables habrá que ejecutar ejecutar el comando `localize`

### Librerías ###

> Si la aplicación hace uso de librerías de terceros, especifiarlas en esta sección

### Mapas ###
> Si la aplicación hace uso de la API de Google Maps, incluir aquí información relativa a los proyectos creados en la consola de Google para poder acceder al API key de los mismos.

* **Debug**: 
	* *[Debug\_project\_name\_goes_here]*
	* *[Debug\_user\_and\_pass\_name\_goes_here]*. (Normalmente las versiones de debug se crean con la cuenta de googleplay.al@mmip.es)
* **Release**:
	* *[Release\_project\_name\_goes_here]*
	* *[Release\_user\_and\_pass\_name\_goes_here]*. (Es posible que sea el cliente quien cree la aplicación de release y nos de permisos a nuestra cuenta googleplay.al@mmip.es. Si no es el caso, la podemos crear con nuestro usuario e invitar al cliente con la cuenta de acceso que nos proporcionen)

### Entornos ###

>Si la aplicación tiene distintos entornos de desarrollo, añade aquí la información referente a los mismos.

* **Debug**: *https://your\_debug\_url\_goes\_here*
* **Release**: *https://your\_release\_url\_goes\_here*

### Usuarios ###

>Si la aplicación necesita usuarios para acceder a las distintas funcionalidades, añade aquí el usuario y el password requerido para ambos entornos (si los tuviese)

* **Debug**: *<your\_debug\_user/your\_debug\_pass>*
* **Release**: *<your\_release\_user/your\_release\_pass>*

### Integración continua ###

>Añadir aquí información relacionada con la integración contínua, tales como la plataforma en donde descargar las versiones de prueba (HockeyApp, Fabric, Google Play Beta, etc.). A ser posible, incluir también enlaces directos a las aplicaciones en las distintas plataformas.

### Certificados ###
>Añadir aquí información relacionada con los certificados con los que se firman las distintas versiones de debug y release de la apliacación. Normalmente los certificados se incluen dentro de un directorio en el raíz del proyecto llamado `/certs`

* **Debug**: *path\_to\_your\_debug\_cert*. (Normalmente en `/certs/debug`)
* **Release**: *path\_to\_your\_release\_cert*. (Normalmente en `/certs/release`)

### Entregas al cliente ###

>Añadir aquí información relacionada con las entregas de versiones al cliente, tales como si el cliente se descarga las versiones desde las distintas plataformas añadidas en la integración continua, si hay que enviarle los ficheros .apk por email, por dropbox, firmados, sin firmar, etc.

### Características específicas ###

>Añadir aquí cualquier consideración específica del proyecto de importancia. Por ejemplo, si se hace uso de alguna arquitectura digna de reseñar o de librerías que requieran cierto conocimiento previo.


